"use strict";

$(function() {

	// #slider
	$(".js-slider-text").sliderPro({
		width: "100%",
		responsive: false,
		autoHeight: true,
		arrows: true,
		buttons: false,
		fade: true,
		autoplay: true,
		autoplayDelay: 3000,
		autoScaleLayers: true,
		loop: true,
		touchSwipe: false,
		slideDistance: 0
	});

	$(".js-slider-videos").sliderPro({
		width: "100%",
		responsive: false,
		autoHeight: true,
		arrows: false,
		buttons: true,
		//fade: true,
		autoplay: true,
		autoplayDelay: 5000,
		autoScaleLayers: true,
		loop: true,
		touchSwipe: false,
		slideDistance: 0
	});


	// #tabs
	var tabs = $(".js-tabs").easytabs({
		tabs: ".videos-nav > ul > li",
		animationSpeed: 700,
		tabActiveClass: "is-active"
	});


	// #show more
	$(".js-show-more").on("click", function(e) {
		var thisis = $(this),
			box = thisis.closest(".videos-content").find(".videosElem_hidden");
			
		e.preventDefault();
		thisis.closest(".videos-more").hide();
		box.removeClass("videosElem_hidden");
	});


	// #map
	var options = {
		zoom: 14,
		panControl: false,
		//zoomControl: false,
		scrollwheel: false,
		mapTypeControl: false,
		center: {lat: 40.3756042, lng: 49.8567215}
	};
	var map = new google.maps.Map(document.getElementById("js-map"), options);
});